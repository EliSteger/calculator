package org.eclipse.example.calc.internal.operations;

public class Multiply {
	public float perform(float arg1, float arg2) {
		return arg1 * arg2;
	}

	public String getName() {
		return "*";
	}
}
